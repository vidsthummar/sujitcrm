<?php
# Clients
$lang['client_vat_number']                       = 'GST Number';
$lang['client_postal_code']                      = 'Pin Code';



# Reports
$lang['report_this_week_leads_conversions']               = 'This Week Inquiries Conversions';

# Leads
$lang['lead_email_already_exists']             = 'Inquiry email already exists in customers data';
$lang['lead_to_client_base_converted_success'] = 'Inquiry converted to customer successfully';
$lang['lead_have_client_profile']              = 'This inquiry have customer profile.';

# Settings
$lang['settings_sales_estimate_prefix']                             = 'Inquiry Number Prefix';
$lang['settings_sales_next_estimate_number']                        = 'Next quotation Number';
$lang['settings_sales_decrement_estimate_number_on_delete']         = 'Decrement quotation number on delete';
$lang['settings_sales_decrement_estimate_number_on_delete_tooltip'] = 'Do you want to decrement the quotation number when the last quotation is deleted? eq. If is set this option to YES and before quotation delete the next quotation number is 15 the next quotation number will decrement to 14.If is set to NO the number will remain to 15. If you have setup delete only on last quotation to NO you should set this option to NO too to keep the next quotation number not decremented.';
$lang['settings_sales_estimate_number_format']                      = 'Inquiry Number Format';
$lang['settings_delete_only_on_last_estimate']                      = 'Delete quotation allowed only on last invoice';
$lang['settings_estimate_auto_convert_to_invoice_on_client_accept']   = 'Auto convert the quotation to invoice after client accept';
$lang['settings_show_sale_agent_on_estimates']      = 'Show Sale Agent On B2B Leadess';
$lang['settings_sales_require_client_logged_in_to_view_estimate'] = 'Require client to be logged in to view quotation';
$lang['show_invoice_estimate_status_on_pdf']                      = 'Show invoice/quotation status on PDF';
$lang['settings_amount_to_words_desc']     = 'Output total amount to words in invoice/quotation';
$lang['settings_leads_kanban_limit']                                    = 'Limit Enquiries kan ban rows per status';
$lang['settings_sales_estimate_prefix']                             = 'Enquiry Number Prefix';
$lang['settings_sales_next_estimate_number']                        = 'Next enquiry Number';
$lang['settings_sales_decrement_estimate_number_on_delete']         = 'Decrement enquiry number on delete';
$lang['settings_sales_decrement_estimate_number_on_delete_tooltip'] = 'Do you want to decrement the enquiry number when the last enquiry is deleted? eq. If is set this option to YES and before estimate delete the next estimate number is 15 the next estimate number will decrement to 14.If is set to NO the number will remain to 15. If you have setup delete only on last estimate to NO you should set this option to NO too to keep the next estimate number not decremented.';
$lang['settings_sales_estimate_number_format']                      = 'Enquiry Number Format';
$lang['settings_delete_only_on_last_estimate']                      = 'Delete enquiry allowed only on last invoice';
$lang['settings_estimate_auto_convert_to_invoice_on_client_accept']   = 'Auto convert the enquiry to invoice after client accept';
$lang['settings_exclude_estimate_from_client_area_with_draft_status'] = 'Exclude enquiries with draft status from customers area';


# B2B Leadess
$lang['estimates']                                = 'Enquiry';
$lang['estimate']                                 = 'Enquiry';
$lang['estimate_lowercase']                       = 'estimate';
$lang['create_new_estimate']                      = 'Create New Enquiry';
$lang['view_estimate']                            = 'View estimate';
$lang['estimate_sent_to_client_success']          = 'The estimate is sent successfully to the client';
$lang['estimate_sent_to_client_fail']             = 'Problem while sending the estimate';
$lang['estimate_view']                            = 'View estimate';
$lang['estimate_select_customer']                 = 'Customer';
$lang['estimate_add_edit_number']                 = 'Enquiry Number';
$lang['estimate_add_edit_date']                   = 'Enquiry Date';
$lang['estimate_invoiced_date']                   = 'Enquiry Invoiced on %s';
$lang['edit_estimate_tooltip']                    = 'Edit Enquiry';
$lang['delete_estimate_tooltip']                  = 'Delete Enquiry';
$lang['estimate_already_send_to_client_tooltip']  = 'This 1uotation is already sent to the client %s';
$lang['estimate_send_to_client_modal_heading']    = 'Send estimate to client';
$lang['estimate_send_to_client_attach_pdf']       = 'Attach estimate PDF';
$lang['estimate_dt_table_heading_number']         = 'Enquiry #';
$lang['estimate_convert_to_invoice_successfully'] = 'Enquiry converted to invoice successfully';
$lang['show_shipping_on_estimate']         = 'Show shipping details in estimate';
$lang['is_invoiced_estimate_delete_error'] = 'This estimate is invoiced. You cant delete the enquiry';
$lang['view_estimate_as_client']         = 'View estimate as customer';
$lang['estimate_status_changed_success'] = 'Enquiry status changed';
$lang['estimate_status_changed_fail']    = 'Failed to change estimate status';
$lang['copy_estimate']                = 'Copy Enquiry';
$lang['estimate_copied_successfully'] = 'Enquiry copied successfully';
$lang['estimate_copied_fail']         = 'Failed to copy estimate';
$lang['estimates_sort_estimate_date']            = 'Enquiry Date';
$lang['estimate_set_reminder_title']             = 'Set Enquiry Reminder';
$lang['show_estimate_reminders_on_calendar']    = 'Enquiry Reminders';
$lang['calendar_estimate_reminder']             = 'Enquiry Reminder';
$lang['estimate_number_exists']                 = 'This estimate number exists for the ongoing year.';
$lang['estimate_files']                           = 'Enquiry Files';
$lang['estimate_items']                          = 'Enquiry Items';
$lang['estimate_not_found']                                 = 'Enquiry not found';
$lang['invoice_activity_auto_converted_from_estimate']      = 'Invoice auto created from enquiry with number %s';
$lang['estimate_due_after']                                      = 'Enquiry Due After (days)';
$lang['not_customer_viewed_estimate']                        = 'An estimate with number %s has been viewed';
$lang['show_pdf_signature_estimate']             = 'Show PDF Signature on Enquiry';
$lang['show_project_on_estimate']                 = 'Show Project Name On Enquiry';
$lang['estimates']                                = 'Enquiry';
$lang['estimate']                                 = 'Enquiry';
$lang['estimate_lowercase']                       = 'estimate';
$lang['create_new_estimate']                      = 'Create New Enquiry';
$lang['view_estimate']                            = 'View estimate';
$lang['estimate_sent_to_client_success']          = 'The estimate is sent successfully to the client';
$lang['estimate_sent_to_client_fail']             = 'Problem while sending the estimate';
$lang['estimate_view']                            = 'View estimate';
$lang['estimate_add_edit_number']                 = 'Enquiry Number';
$lang['estimate_add_edit_date']                   = 'Enquiry Date';
$lang['estimate_invoiced_date']                   = 'Enquiry Invoiced on %s';
$lang['edit_estimate_tooltip']                    = 'Edit Enquiry';
$lang['delete_estimate_tooltip']                  = 'Delete Enquiry';
$lang['estimate_sent_to_email_tooltip']           = 'Send to Email';
$lang['estimate_already_send_to_client_tooltip']  = 'This estimate is already sent to the client %s';
$lang['estimate_view_activity_tooltip']           = 'Activity Log';
$lang['estimate_send_to_client_modal_heading']    = 'Send estimate to client';
$lang['estimate_send_to_client_attach_pdf']       = 'Attach estimate PDF';
$lang['estimate_send_to_client_preview_template'] = 'Preview Email Template';
$lang['estimate_dt_table_heading_number']         = 'Enquiry #';
$lang['estimate_pdf_heading']            = 'Enquiry';
$lang['estimate_data_date']              = 'Enquiry Date';
$lang['clients_estimate_dt_number']             = 'Enquiry #';
$lang['clients_estimate_invoiced_successfully'] = 'Thank you for accepting the estimate. Please review the created invoice for the estimate';
$lang['clients_estimate_accepted_not_invoiced'] = 'Thank you for accepting this estimate';
$lang['clients_estimate_declined']              = 'Enquiry declined. You can accept the enquiry any time before expiry date';
$lang['clients_estimate_failed_action']         = 'Failed to take action on this estimate';
$lang['calendar_estimate']          = 'Enquiry';
$lang['settings_show_sale_agent_on_estimates']      = 'Show Sale Agent On Enquiry';
$lang['custom_field_estimate']    = 'Enquiry';

# Proposals
$lang['proposal_convert_estimate']              = 'Enquiry';
$lang['proposal_convert_to_estimate']           = 'Convert to estimate';
$lang['proposal_converted_to_estimate_success'] = 'Proposal converted to estimate successfully';
$lang['proposal_converted_to_estimate_fail']    = 'Failed to convert proposal to estimate';

# Clients
$lang['clients_estimate_dt_number']             = 'Enquiry #';
$lang['clients_estimate_invoiced_successfully'] = 'Thank you for accepting the estimate. Please review the created invoice for the estimate';
$lang['clients_estimate_accepted_not_invoiced'] = 'Thank you for accepting this estimate';
$lang['clients_estimate_declined']              = 'Enquiry declined. You can accept the enquiry any time before expiry date';
$lang['clients_estimate_failed_action']         = 'Failed to take action on this estimate';

# Custom fields
$lang['custom_field_estimate']    = 'Enquiry';

# Notifications & Leads/B2B Leadess/Invoices Activity Log
$lang['not_estimate_status_updated']                  = 'Enquiry Status Updated: From: %s to %s';
$lang['not_estimate_customer_accepted']                           = 'Congratulations! Client accepted enquiry with number %s';
$lang['not_estimate_customer_declined']                           = 'Client declined estimate with number %s';
$lang['estimate_activity_converted']                              = 'converted this estimate to invoice.<br /> %s';
$lang['estimate_activity_created']                                = 'Created the estimate';
$lang['estimate_activity_number_changed']                         = 'Enquiry number changed from %s to %s';
$lang['invoice_estimate_activity_sent_to_client']                 = 'sent estimate to client';
$lang['estimate_activity_client_accepted_and_converted']          = 'Customer accepted this enquiry. Enquiry is converted to invoice with number %s';
$lang['estimate_activity_client_accepted']                        = 'Customer accepted this enquiry';
$lang['estimate_activity_client_declined']                        = 'Client declined this enquiry';
$lang['estimate_activity_marked']                                 = 'marked estimate as %s';

$lang['purchaseorder_activity_created']                                = 'Created the Purchase Order';
$lang['purchaseorder_activity_number_changed']                         = 'Purchase Order number changed from %s to %s';
$lang['purchaseorder_activity_converted']                              = 'converted this purchase order to invoice.<br /> %s';
$lang['not_purchaseorder_status_updated']                  = 'Purchase Order Status Updated: From: %s to %s';


# Home stats
$lang['home_estimate_overview']       = 'Enquiry overview';
$lang['home_lead_overview']           = 'Enquiries Overview';

# Utilities - BULK PDF Exporter
$lang['bulk_export_pdf_estimates']     = 'B2B Leadess';


$lang['item_description_placeholder']           = 'Part No.';
