<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php echo form_open('admin/invoice_items/manage',array('id'=>'invoice_item_form')); ?>
<?php echo form_hidden('itemid'); ?>
<div class="modal fade" id="sales_item_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="edit-title"><?php echo _l('invoice_item_edit_heading'); ?></span>
                    <span class="add-title"><?php echo _l('invoice_item_add_heading'); ?></span>
                </h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-warning affect-warning hide">
                            <?php echo _l('changing_items_affect_warning'); ?>
                        </div>
                        <?php
                        $orgkey='';
                        foreach ($_REQUEST as $key => $asd) 
                        {
                           $orgkey=$key;
                        }
                        $eorf=explode('/',$orgkey);
                        $torf=$eorf[(count($eorf)-1)];
                        ?>
                        <?php echo render_input('description','Name'); ?>
                        <?php //echo render_input('hsn_code','HSN Code'); ?>
                        <?php echo render_textarea('long_description','Product Model'); ?>
                        <?php //echo render_input('code','Product Code'); ?>
                        <!-- <?php echo render_input('hsn','HSN'); ?>
                        <?php echo render_input('cost','Cost Price'); ?>
                        <?php echo render_input('warenty','Warenty'); ?>
                        <?php echo render_input('upto','Valid Upto'); ?> -->
                        <div class="form-group">
                        <label for="hsn" class="control-label"> HSN </label>
                            <input type="text" id="hsn" name="hsn" class="form-control" value="">
                        </div>
                        <input type="hidden" id="page_from" name="page_from" class="form-control" value="<?php echo $torf;?>">
                        <div class="form-group">
                        <label for="cost" class="control-label"> Cost Price </label>
                            <input type="number" id="cost" name="cost" class="form-control" value="">
                        </div>
                       
                        <div class="form-group">
                        <label for="warenty" class="control-label"> Warranty </label>
                            <input type="text" id="warenty" name="warenty" class="form-control" value="">
                        </div>
                        <div class="form-group">
                        <label for="upto" class="control-label"> Valid Upto </label>
                            <input type="date" id="upto" name="upto" class="form-control" value="">
                        </div>
                        <?php
                            foreach($currencies as $currency){
                                if($currency['isdefault'] == 0 && total_rows(db_prefix().'clients',array('default_currency'=>$currency['id'])) > 0){ ?>
                                <div class="form-group">
                                    <label for="rate_currency_<?php echo $currency['id']; ?>" class="control-label">
                                        <?php echo _l('invoice_item_add_edit_rate_currency', $currency['name']); ?></label>
                                        <input type="number" id="rate_currency_<?php echo $currency['id']; ?>" name="rate_currency_<?php echo $currency['id']; ?>" class="form-control" value="">
                                    </div>
                             <?php   }
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-6">
                             <div class="form-group">
                                <label class="control-label" for="tax"><?php echo _l('tax_1'); ?></label>
                                <select class="selectpicker display-block" data-width="100%" name="tax" data-none-selected-text="<?php echo _l('no_tax'); ?>">
                                    <option value=""></option>
                                    <?php foreach($taxes as $tax){ ?>
                                    <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                         <div class="form-group">
                            <label class="control-label" for="tax2"><?php echo _l('tax_2'); ?></label>
                            <select class="selectpicker display-block" disabled data-width="100%" name="tax2" data-none-selected-text="<?php echo _l('no_tax'); ?>">
                                <option value=""></option>
                                <?php foreach($taxes as $tax){ ?>
                                <option value="<?php echo $tax['id']; ?>" data-subtext="<?php echo $tax['name']; ?>"><?php echo $tax['taxrate']; ?>%</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix mbot15"></div>
                <?php echo render_select('unit',$items_units,array('name','name'),'Item Unit'); ?>
                <div class="form-group">
                        <label for="rate" class="control-label">
                          <?php echo _l('invoice_item_add_edit_rate_currency',$base_currency->name . ' <small>('._l('base_currency_string').')</small>'); ?></label>
                            <input type="number" id="rate" name="rate" class="form-control" value="">
                        </div>
                <div id="custom_fields_items">
                    <?php echo render_custom_fields('items'); ?>
                </div>
                <?php echo render_select('group_id',$items_groups,array('id','name'),'item_group'); ?>
                <?php echo render_select('brand_id',$items_brands,array('id','name'),'Item Brand'); ?>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
        <button type="submit" class="btn btn-info"><?php echo _l('submit'); ?></button>
    </div>
</div>
</div>
</div>
<?php echo form_close(); ?>
<script>
    // Maybe in modal? Eq convert to invoice or convert proposal to estimate/invoice
    if(typeof(jQuery) != 'undefined'){
        init_item_js();
    } else {
     window.addEventListener('load', function () {
       var initItemsJsInterval = setInterval(function(){
            if(typeof(jQuery) != 'undefined') {
                init_item_js();
                clearInterval(initItemsJsInterval);
            }
         }, 1000);
     });
  }
// Items add/edit
function manage_invoice_items(form) {
    var data = $(form).serialize();

    var url = form.action;
    $.post(url, data).done(function (response) {
        response = JSON.parse(response);
        if (response.success == true) {
            var item_select = $('#item_select');
            if ($("body").find('.accounting-template').length > 0) {
                if (!item_select.hasClass('ajax-search')) {
                    var group = item_select.find('[data-group-id="' + response.item.group_id + '"]');
                    var brand = item_select.find('[data-brand-id="' + response.item.brand_id + '"]');
                    if (group.length == 0 || brand.length == 0) {
                        var _option = '<optgroup label="' + (response.item.group_name == null ? '' : response.item.group_name) + '" data-group-id="' + response.item.group_id + '">' + _option + '</optgroup>';
                        var _option = '<optgroup label="' + (response.item.brand_name == null ? '' : response.item.brand_name) + '" data-brand-id="' + response.item.brand_id + '">' + _option + '</optgroup>';
                        if (item_select.find('[data-group-id="0"]').length == 0 || item_select.find('[data-brand-id="0"]').length == 0) {
                            item_select.find('option:first-child').after(_option);
                        } else {
                            item_select.find('[data-group-id="0"]').after(_option);
                            item_select.find('[data-brand-id="0"]').after(_option);
                        }
                    } else {

                        group.prepend('<option data-subtext="' + response.item.long_description + '" value="' + response.item.itemid + '">(' + accounting.formatNumber(response.item.rate) + ') ' + response.item.description + '</option>');
                        brand.prepend('<option data-subtext="' + response.item.long_description + '" value="' + response.item.itemid + '">(' + accounting.formatNumber(response.item.rate) + ') ' + response.item.description + '</option>');
                    }
                }
                if (!item_select.hasClass('ajax-search')) {
                    item_select.selectpicker('refresh');
                } else {

                    item_select.contents().filter(function () {
                        return !$(this).is('.newitem') && !$(this).is('.newitem-divider');
                    }).remove();

                    var clonedItemsAjaxSearchSelect = item_select.clone();
                    item_select.selectpicker('destroy').remove();
                    $("body").find('.items-select-wrapper').append(clonedItemsAjaxSearchSelect);
                    init_ajax_search('items', '#item_select.ajax-search', undefined, admin_url + 'items/search');
                }

                add_item_to_preview(response.item.itemid);
            } else {
                // Is general items view
                $('.table-invoice-items').DataTable().ajax.reload(null, false);
            }
            alert_float('success', response.message);
        }
        $('#sales_item_modal').modal('hide');
    }).fail(function (data) {
        alert_float('danger', data.responseText);
    });
    return false;
}
function init_item_js() {
     // Add item to preview from the dropdown for invoices estimates
    $("body").on('change', 'select[name="item_select"]', function () {
        var itemid = $(this).selectpicker('val');
        var dc=0;
        if (itemid != '') {
            $('#afterselection').show();
            $('#beforeselection').hide();
            if(dc==0)
            {
                setTimeout(function() {
                    add_item_to_preview(itemid);
                    $('#afterselection').hide();
                    $('#beforeselection').show();
                }, 1000);
                
            }
        }
        dc++;
    });

    // Items modal show action
    $("body").on('show.bs.modal', '#sales_item_modal', function (event) {



        $('.affect-warning').addClass('hide');

        var $itemModal = $('#sales_item_modal');
        $('input[name="itemid"]').val('');
        $itemModal.find('input').not('input[type="hidden"]').val('');
        $itemModal.find('textarea').val('');
        $itemModal.find('select').selectpicker('val', '').selectpicker('refresh');
        $('select[name="tax2"]').selectpicker('val', '').change();
        $('select[name="tax"]').selectpicker('val', '').change();
        $itemModal.find('.add-title').removeClass('hide');
        $itemModal.find('.edit-title').addClass('hide');

        var id = $(event.relatedTarget).data('id');
        // If id found get the text from the datatable
        if (typeof (id) !== 'undefined') {

            $('.affect-warning').removeClass('hide');
            $('input[name="itemid"]').val(id);
             $('input[name="page_from"]').val('<?php echo $torf;?>');

            requestGetJSON('invoice_items/get_item_by_id/' + id).done(function (response) {
                $itemModal.find('input[name="description"]').val(response.description);
                $itemModal.find('input[name="hsn"]').val(response.hsn);
                $itemModal.find('input[name="cost"]').val(response.cost);
                $itemModal.find('input[name="upto"]').val(response.upto);
                $itemModal.find('input[name="warenty"]').val(response.warenty);
                $itemModal.find('textarea[name="long_description"]').val(response.long_description.replace(/(<|<)br\s*\/*(>|>)/g, " "));
                $itemModal.find('input[name="rate"]').val(response.rate);
                $('select[name="unit"]').selectpicker('val', response.unit);
                $('select[name="tax"]').selectpicker('val', response.taxid).change();
                $('select[name="tax2"]').selectpicker('val', response.taxid_2).change();
                $itemModal.find('#group_id').selectpicker('val', response.group_id);
                $itemModal.find('#brand_id').selectpicker('val', response.brand_id);
                $itemModal.find('#unit_id').selectpicker('val', response.unit_id);
                $.each(response, function (column, value) {
                    if (column.indexOf('rate_currency_') > -1) {
                        $itemModal.find('input[name="' + column + '"]').val(value);
                    }
                });

                $('#custom_fields_items').html(response.custom_fields_html);

                init_selectpicker();
                init_color_pickers();
                init_datepicker();

                $itemModal.find('.add-title').addClass('hide');
                $itemModal.find('.edit-title').removeClass('hide');
                validate_item_form();
            });

        }
        else
        {
            $('#warenty').val('365');
            $('#page_from').val('<?php echo $torf;?>');
        }
    });

    $("body").on("hidden.bs.modal", '#sales_item_modal', function (event) {
        $('#item_select').selectpicker('val', '');
    });

   validate_item_form();
}
function validate_item_form(){
    // Set validation for invoice item form
    _validate_form($('#invoice_item_form'), {
        description: 'required',
        rate: {
            required: true,
        }
    }, manage_invoice_items);
}
</script>
