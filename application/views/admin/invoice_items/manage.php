<?php init_head(); ?>
<div id="wrapper">
  <div class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="panel_s">
          <div class="panel-body">
           <?php do_action('before_items_page_content'); ?>
           <?php if(has_permission('items','','create')){ ?>
           <div class="_buttons">
            <a href="#" class="btn btn-info pull-left" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#sales_item_modal"><?php echo _l('new_invoice_item'); ?></a>
            <a href="#" class="btn btn-info pull-left mleft5" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#groups"><?php echo _l('item_groups'); ?></a>
               <a href="<?php echo admin_url('invoice_items/import'); ?>" class="btn btn-info pull-left mleft5"><?php echo _l('import'); ?></a>
            <a href="#" class="btn btn-info pull-left mleft5" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#brands"><?php echo 'BRANDS'; ?></a>
            <a href="#" class="btn btn-info pull-left mleft5" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#units"><?php echo 'UNITS'; ?></a>
          </div>
          <div class="clearfix"></div>
          <hr class="hr-panel-heading" />
          <?php } ?>
          <?php
          $table_data = array(
            'Name',
            'Model',
            'HSN',
            'Brand',
            'Group',
            'Price',
            _l('tax_1'),
            _l('tax_2'),
            _l('unit'),
            'Validity Date',
            'Warrenty',
            /*_l('item_group_name'),
            'Brand Name',*/
          );
            $cf = get_custom_fields('items');
            foreach($cf as $custom_field) {
                array_push($table_data,$custom_field['name']);
            }
            render_datatable($table_data,'invoice-items'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('admin/invoice_items/item'); ?>
<div class="modal fade" id="units" tabindex="-1" role="dialog" aria-labelledby="myModalLabelunit">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelunit">
          <?php echo 'Units'; ?>
        </h4>
      </div>
      <div class="modal-body">
        <?php if(has_permission('items','','create')){ ?>
        <div class="input-group">
          <input type="text" name="item_unit_name" id="item_unit_name" class="form-control" placeholder="<?php echo 'UNIT'; ?>">
          <span class="input-group-btn">
            <button class="btn btn-info p7" type="button" id="new-item-unit-insert"><?php echo 'NEW UNIT'; ?></button>
          </span>
        </div>
        <hr />
        <?php } ?>
        <div class="row">
         <div class="container-fluid">
          <table class="table dt-table table-items-units" data-order-col="0" data-order-type="asc">
            <thead>
              <tr>
                <th><?php echo 'ID'; ?></th>
                <th><?php echo 'NAME'; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($items_units as $unit){ ?>
              <tr class="row-has-options" data-unit-row-id="<?php echo $unit['id']; ?>">
                <td data-order="<?php echo $unit['id']; ?>"><?php echo $unit['id']; ?></td>
                <td data-order="<?php echo $unit['name']; ?>">
                  <span class="unit_name_plain_text"><?php echo $unit['name']; ?></span>
                  <div class="unit_edit hide">
                   <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-info p8 update-item-unit" type="button"><?php echo _l('submit'); ?></button>
                    </span>
                  </div>
                </div>
                <div class="row-options">
                  <?php if(has_permission('items','','edit')){ ?>
                    <a href="#" class="edit-item-unit">
                      <?php echo _l('edit'); ?>
                    </a>
                    <?php } ?>
                    <?php if(has_permission('items','','delete')){ ?>
                      | <a href="<?php echo admin_url('invoice_items/delete_unit/'.$unit['id']); ?>" class="delete-item-unit _delete text-danger">
                        <?php echo _l('delete'); ?>
                      </a>
                      <?php } ?>
                    </div>
              </td>

              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="brands" tabindex="-1" role="dialog" aria-labelledby="myModalLabelbrand">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabelbrand">
          <?php echo 'BRANDS'; ?>
        </h4>
      </div>
      <div class="modal-body">
        <?php if(has_permission('items','','create')){ ?>
        <div class="input-group">
          <input type="text" name="item_brand_name" id="item_brand_name" class="form-control" placeholder="<?php echo 'BRAND'; ?>">
          <span class="input-group-btn">
            <button class="btn btn-info p7" type="button" id="new-item-brand-insert"><?php echo 'NEW BRAND'; ?></button>
          </span>
        </div>
        <hr />
        <?php } ?>
        <div class="row">
         <div class="container-fluid">
          <table class="table dt-table table-items-brands" data-order-col="0" data-order-type="asc">
            <thead>
              <tr>
                <th><?php echo 'ID'; ?></th>
                <th><?php echo 'NAME'; ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($items_brands as $brand){ ?>
              <tr class="row-has-options" data-brand-row-id="<?php echo $brand['id']; ?>">
                <td data-order="<?php echo $brand['id']; ?>"><?php echo $brand['id']; ?></td>
                <td data-order="<?php echo $brand['name']; ?>">
                  <span class="brand_name_plain_text"><?php echo $brand['name']; ?></span>
                  <div class="brand_edit hide">
                   <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-info p8 update-item-brand" type="button"><?php echo _l('submit'); ?></button>
                    </span>
                  </div>
                </div>
                <div class="row-options">
                  <?php if(has_permission('items','','edit')){ ?>
                    <a href="#" class="edit-item-brand">
                      <?php echo _l('edit'); ?>
                    </a>
                    <?php } ?>
                    <?php if(has_permission('items','','delete')){ ?>
                      | <a href="<?php echo admin_url('invoice_items/delete_brand/'.$brand['id']); ?>" class="delete-item-brand _delete text-danger">
                        <?php echo _l('delete'); ?>
                      </a>
                      <?php } ?>
                    </div>
              </td>

              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="groups" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
          <?php echo _l('item_groups'); ?>
        </h4>
      </div>
      <div class="modal-body">
        <?php if(has_permission('items','','create')){ ?>
        <div class="input-group">
          <input type="text" name="item_group_name" id="item_group_name" class="form-control" placeholder="<?php echo _l('item_group_name'); ?>">
          <span class="input-group-btn">
            <button class="btn btn-info p7" type="button" id="new-item-group-insert"><?php echo _l('new_item_group'); ?></button>
          </span>
        </div>
        <hr />
        <?php } ?>
        <div class="row">
         <div class="container-fluid">
          <table class="table dt-table table-items-groups" data-order-col="0" data-order-type="asc">
            <thead>
              <tr>
                <th><?php echo 'ID'; ?></th>
                <th><?php echo _l('item_group_name'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($items_groups as $group){ ?>
              <tr class="row-has-options" data-group-row-id="<?php echo $group['id']; ?>">
                <td data-order="<?php echo $group['id']; ?>"><?php echo $group['id']; ?></td>
                <td data-order="<?php echo $group['name']; ?>">
                  <span class="group_name_plain_text"><?php echo $group['name']; ?></span>
                  <div class="group_edit hide">
                   <div class="input-group">
                    <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button class="btn btn-info p8 update-item-group" type="button"><?php echo _l('submit'); ?></button>
                    </span>
                  </div>
                </div>
                <div class="row-options">
                  <?php if(has_permission('items','','edit')){ ?>
                    <a href="#" class="edit-item-group">
                      <?php echo _l('edit'); ?>
                    </a>
                    <?php } ?>
                    <?php if(has_permission('items','','delete')){ ?>
                      | <a href="<?php echo admin_url('invoice_items/delete_group/'.$group['id']); ?>" class="delete-item-group _delete text-danger">
                        <?php echo _l('delete'); ?>
                      </a>
                      <?php } ?>
                    </div>
              </td>

              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo _l('close'); ?></button>
    </div>
  </div>
</div>
</div>
<?php init_tail(); ?>
<script>
  $(function(){
    initDataTable('.table-invoice-items', admin_url+'invoice_items/table', undefined, undefined,'undefined',[0,'asc']);
    if(get_url_param('groups_modal')){
       // Set time out user to see the message
       setTimeout(function(){
         $('#groups').modal('show');
       },1000);
    }

     if(get_url_param('brands_modal')){
       // Set time out user to see the message
       setTimeout(function(){
         $('#brands').modal('show');
       },1000);
    }

    if(get_url_param('units_modal')){
       // Set time out user to see the message
       setTimeout(function(){
         $('#units').modal('show');
       },1000);
    }

    $('#new-item-group-insert').on('click',function(){
      var group_name = $('#item_group_name').val();
      if(group_name != ''){
          $.post(admin_url+'invoice_items/add_group',{name:group_name}).done(function(){
           window.location.href = admin_url+'invoice_items?groups_modal=true';
         });
      }
    });

    $('#new-item-brand-insert').on('click',function(){
      var brand_name = $('#item_brand_name').val();
      if(brand_name != ''){
          $.post(admin_url+'invoice_items/add_brand',{name:brand_name}).done(function(){
           window.location.href = admin_url+'invoice_items?brands_modal=true';
         });
      }
    });

    $('#new-item-unit-insert').on('click',function(){
      var unit_name = $('#item_unit_name').val();
      if(unit_name != ''){
          $.post(admin_url+'invoice_items/add_unit',{name:unit_name}).done(function(){
           window.location.href = admin_url+'invoice_items?units_modal=true';
         });
      }
    });

    $('body').on('click','.edit-item-unit',function(e){
      e.preventDefault();
      var tr = $(this).parents('tr'),
      unit_id = tr.attr('data-unit-row-id');
      tr.find('.unit_name_plain_text').toggleClass('hide');
      tr.find('.unit_edit').toggleClass('hide');
      tr.find('.unit_edit input').val(tr.find('.unit_name_plain_text').text());
    });

    $('body').on('click','.update-item-unit',function(){
      var tr = $(this).parents('tr');
      var unit_id = tr.attr('data-unit-row-id');
      name = tr.find('.unit_edit input').val();
      if(name != ''){
        $.post(admin_url+'invoice_items/update_unit/'+unit_id,{name:name}).done(function(){
         window.location.href = admin_url+'invoice_items';
       });
      }
    });

    $('body').on('click','.edit-item-brand',function(e){
      e.preventDefault();
      var tr = $(this).parents('tr'),
      brand_id = tr.attr('data-brand-row-id');
      tr.find('.brand_name_plain_text').toggleClass('hide');
      tr.find('.brand_edit').toggleClass('hide');
      tr.find('.brand_edit input').val(tr.find('.brand_name_plain_text').text());
    });

    $('body').on('click','.update-item-brand',function(){
      var tr = $(this).parents('tr');
      var brand_id = tr.attr('data-brand-row-id');
      name = tr.find('.brand_edit input').val();
      if(name != ''){
        $.post(admin_url+'invoice_items/update_brand/'+brand_id,{name:name}).done(function(){
         window.location.href = admin_url+'invoice_items';
       });
      }
    });

    $('body').on('click','.edit-item-group',function(e){
      e.preventDefault();
      var tr = $(this).parents('tr'),
      group_id = tr.attr('data-group-row-id');
      tr.find('.group_name_plain_text').toggleClass('hide');
      tr.find('.group_edit').toggleClass('hide');
      tr.find('.group_edit input').val(tr.find('.group_name_plain_text').text());
    });

    $('body').on('click','.update-item-group',function(){
      var tr = $(this).parents('tr');
      var group_id = tr.attr('data-group-row-id');
      name = tr.find('.group_edit input').val();
      if(name != ''){
        $.post(admin_url+'invoice_items/update_group/'+group_id,{name:name}).done(function(){
         window.location.href = admin_url+'invoice_items';
       });
      }
    });
  });
</script>
</body>
</html>
